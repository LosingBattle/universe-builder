做了一个简单的go generate工具，根据原有结构体生成对应的builder结构体,内置flag标识,区分指针类型是否赋值

# 用法
go install gitee.com/LosingBattle/universe-builder@latest

添加 go:generate  universe-builder -type struct

```go
//go:generate  universe-builder -type struct
package test

type Foo struct {
	A *int //note
	B *int
}


```
foo.go
会生成一个文件foo_builder.go
```go

package test

type FooBuilder struct {
	a     int //note
	aFlag bool
	b     int
	bFlag bool
}

func NewFooBuilder() *FooBuilder {
	return &FooBuilder{}
}

func (builder *FooBuilder) A(a int) *FooBuilder {
	builder.a = a
	builder.aFlag = true
	return builder
}
func (builder *FooBuilder) B(b int) *FooBuilder {
	builder.b = b
	builder.bFlag = true
	return builder
}

func (builder *FooBuilder) Build() *Foo {
	req := &Foo{}
	if builder.aFlag {
		req.A = &builder.a
	}
	if builder.bFlag {
		req.B = &builder.b
	}
	return req
}


```

